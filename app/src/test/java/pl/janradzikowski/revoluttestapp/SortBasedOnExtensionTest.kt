package pl.janradzikowski.revoluttestapp

import junit.framework.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SortBasedOnExtensionTest {

    @Test
    fun `should return A, B, C, D, when first list contains A, B, C, D and second D, B, C, A`() {
        val first = listOf("A", "B", "C", "D")
        val second = listOf("D", "B", "C", "A")
        val output = second.sortBasedOn(first)

        assertTrue(output == first)
    }
}