package pl.janradzikowski.revoluttestapp.di

import org.koin.dsl.module
import pl.janradzikowski.revoluttestapp.infrastructure.CurrencyInfoMapper
import pl.janradzikowski.revoluttestapp.infrastructure.CurrencyMapper

val mappersModule = module {
    factory { CurrencyInfoMapper(get()) }
    factory { CurrencyMapper() }
}