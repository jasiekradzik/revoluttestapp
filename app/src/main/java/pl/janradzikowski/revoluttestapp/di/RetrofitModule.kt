package pl.janradzikowski.revoluttestapp.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import pl.janradzikowski.revoluttestapp.domain.CurrencyInfoRepository
import pl.janradzikowski.revoluttestapp.infrastructure.DataMapper
import pl.janradzikowski.revoluttestapp.infrastructure.retrofit.*
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val PARTNER_API_URL = "https://hiring.revolut.codes/api/android/"

val retrofitModule = module {
    single { createRetrofit(get(), get(), get()) }
    single { HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) } }
    single { createOkHttpClient(get()) }
    single { createMoshi() }
    single { createConverterFactory(get()) }
    single { RetrofitErrorMapper(get()) as DataMapper<Throwable, Throwable> }
    single { createCallAdapterFactory(get()) }
    single { createCurrencyInfoApi(get()) }
    single { createValidationErrorConverter(get()) }
    single { RetrofitCurrencyRatesRepository(get(), get()) as CurrencyInfoRepository }
}

private fun createRetrofit(httpClient: OkHttpClient, converterFactory: Converter.Factory, callAdapterFactory: CallAdapter.Factory): Retrofit  =
    Retrofit.Builder()
        .baseUrl(PARTNER_API_URL)
        .client(httpClient)
        .addConverterFactory(converterFactory)
        .addCallAdapterFactory(callAdapterFactory)
        .build()

private fun createConverterFactory(moshi: Moshi): Converter.Factory =
    MoshiConverterFactory.create(moshi)

private fun createCallAdapterFactory(errorMapper: DataMapper<Throwable, Throwable>) : CallAdapter.Factory =
    RxJava3ErrorHandlingCallAdapterFactory(errorMapper)

private fun createOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient =
    OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .build()

private fun createValidationErrorConverter(converterFactory: Converter.Factory): Converter<ResponseBody, ValidationError> =
    ValidationErrorConverter(
        converterFactory.responseBodyConverter(
            ValidationError::class.java,
            arrayOfNulls<Annotation>(0),
            null
        ) as Converter<ResponseBody, ValidationError>
    )

private fun createCurrencyInfoApi(retrofit: Retrofit): CurrencyInfoApi = retrofit.create(CurrencyInfoApi::class.java)

private fun createMoshi(): Moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()
