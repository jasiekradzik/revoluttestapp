package pl.janradzikowski.revoluttestapp.di

import org.koin.dsl.module
import pl.janradzikowski.revoluttestapp.domain.LoadCurrencyInfoUseCase

val domainModule = module {
    single { LoadCurrencyInfoUseCase(get()) }
}