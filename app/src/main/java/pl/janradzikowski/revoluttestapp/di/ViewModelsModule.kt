package pl.janradzikowski.revoluttestapp.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pl.janradzikowski.revoluttestapp.presentation.CurrencyRatesViewModel

val viewModelsModule = module {
    viewModel { CurrencyRatesViewModel(get()) }
}