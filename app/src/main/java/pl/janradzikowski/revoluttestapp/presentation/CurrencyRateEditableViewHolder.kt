package pl.janradzikowski.revoluttestapp.presentation

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_currency_rate_editable.view.*
import pl.janradzikowski.revoluttestapp.domain.CurrencyRateEditableItem

class CurrencyRateEditableViewHolder(itemView: View, val valueChanged: (item: CurrencyRateEditableItem) -> Unit): RecyclerView.ViewHolder(itemView) {
    fun bind(item: CurrencyRateEditableItem) {
        with(itemView) {
            currencyIcon.setImageResource(item.currency.iconRes)
            currencyCode.text = item.currency.name
            currencyDescription.setText(item.currency.descriptionResId)
            if (item.resetInputValue) currencyValue.setText(item.inputValue)
            if (item.requestFocus) {
                currencyValue.requestFocus()
                item.requestFocus = false
            }
            currencyValue.addTextChangedListener(object: TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {  }
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (item.resetInputValue) {
                        item.resetInputValue = false
                    } else {
                        item.inputValue = s.toString()
                        valueChanged(item)
                    }
                }
                override fun afterTextChanged(s: Editable?) {}
            })
        }
    }
}