package pl.janradzikowski.revoluttestapp.presentation

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_currency_rate_display.view.*
import pl.janradzikowski.revoluttestapp.domain.CurrencyRateDisplayItem

class CurrencyRateDisplayViewHolder(itemView: View, val itemClicked: (CurrencyRateDisplayItem) -> Unit): RecyclerView.ViewHolder(itemView) {
    fun bind(item: CurrencyRateDisplayItem) {
        with(itemView) {
            currencyIcon.setImageResource(item.currencyRate.currency.iconRes)
            currencyCode.text = item.currencyRate.currency.name
            currencyDescription.setText(item.currencyRate.currency.descriptionResId)
            currencyValue.text = item.currencyValue
            container.setOnClickListener { itemClicked(item) }
            currencyValue.setOnClickListener { itemClicked(item) }
        }
    }

    fun setCurrencyValue(item: CurrencyRateDisplayItem) {
        itemView.currencyValue.text = item.currencyValue
    }
}