package pl.janradzikowski.revoluttestapp.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pl.janradzikowski.revoluttestapp.R
import pl.janradzikowski.revoluttestapp.domain.CurrencyRateDisplayItem
import pl.janradzikowski.revoluttestapp.domain.CurrencyRateEditableItem
import pl.janradzikowski.revoluttestapp.domain.CurrencyRateItem

private const val CURRENCY_ITEM_DISPLAY = 0
private const val CURRENCY_ITEM_EDITABLE = 1

class CurrencyRateListAdapter(
    private val valueChanged: (item: CurrencyRateEditableItem) -> Unit,
    private val itemClicked: (CurrencyRateDisplayItem) -> Unit
) : ListAdapter<CurrencyRateItem, RecyclerView.ViewHolder>(DIFF_CALLBACK) {
    var data = mutableListOf<CurrencyRateItem>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun getItemViewType(position: Int) =
        when(data[position]) {
            is CurrencyRateEditableItem -> CURRENCY_ITEM_EDITABLE
            else -> CURRENCY_ITEM_DISPLAY
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        CURRENCY_ITEM_EDITABLE -> CurrencyRateEditableViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_currency_rate_editable, parent, false), valueChanged)
        else -> CurrencyRateDisplayViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_currency_rate_display, parent, false), itemClicked)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = when (holder) {
        is CurrencyRateEditableViewHolder -> holder.bind(data[position] as CurrencyRateEditableItem)
        is CurrencyRateDisplayViewHolder -> holder.bind(data[position] as CurrencyRateDisplayItem)
        else -> throw Exception("Unsupported View Holder")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        when {
            payloads.isEmpty() && holder is CurrencyRateDisplayViewHolder -> holder.bind(data[position] as CurrencyRateDisplayItem)
            payloads.isNotEmpty() && holder is CurrencyRateDisplayViewHolder -> holder.setCurrencyValue(data[position] as CurrencyRateDisplayItem)
            else -> super.onBindViewHolder(holder, position, payloads)
        }
    }

    enum class CurrencyRatePayload {
        DISPLAY
    }
}

private val DIFF_CALLBACK: DiffUtil.ItemCallback<CurrencyRateItem> = object : DiffUtil.ItemCallback<CurrencyRateItem>() {
    override fun areItemsTheSame(oldItem: CurrencyRateItem, newItem: CurrencyRateItem) =
        oldItem.provideCurrency() == newItem.provideCurrency()

    override fun areContentsTheSame(oldItem: CurrencyRateItem, newItem: CurrencyRateItem): Boolean =
        when {
            oldItem is CurrencyRateDisplayItem && newItem is CurrencyRateDisplayItem && oldItem.currencyValue == newItem.currencyValue -> true
            oldItem is CurrencyRateEditableItem && newItem is CurrencyRateEditableItem && oldItem.currency == newItem.currency -> true
            else -> false
        }
}