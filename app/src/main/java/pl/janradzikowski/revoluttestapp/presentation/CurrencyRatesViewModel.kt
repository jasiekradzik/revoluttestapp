package pl.janradzikowski.revoluttestapp.presentation

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import com.futuremind.liverelay.LiveStateRelay
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.processors.PublishProcessor
import io.reactivex.rxjava3.schedulers.Schedulers
import pl.janradzikowski.revoluttestapp.*
import pl.janradzikowski.revoluttestapp.domain.*
import pl.janradzikowski.revoluttestapp.presentation.CurrencyRatesViewModel.RefreshCurrencyRatesState.*
import pl.janradzikowski.revoluttestapp.presentation.CurrencyRatesViewModel.RefreshType.Force
import pl.janradzikowski.revoluttestapp.presentation.CurrencyRatesViewModel.RefreshType.Timed
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

private val BASE_CURRENCY_DEFAULT = Currency.EUR
private const val REFRESH_RATE = 1L


class CurrencyRatesViewModel(
    private val loadCurrencyInfoUseCase: LoadCurrencyInfoUseCase
) : ViewModel() {

    var currencyRateItems = mutableListOf<CurrencyRateItem>()
        set(value) {
            field.clear()
            field.addAll(value)
        }
    private var baseCurrency = BASE_CURRENCY_DEFAULT
    private val refreshCurrencyRatesState = LiveStateRelay<RefreshCurrencyRatesState>()
    private val refreshCurrencyRatesProcessor = PublishProcessor.create<RefreshType>()
    private var timedRefreshDisposable: Disposable? = null
    private var loadCurrencyRatesDisposable: Disposable? = null
    private val decideWhenToRefreshDisposable: Disposable = refreshCurrencyRatesProcessor
        .subscribe { state ->
        when(state) {
            is Force -> loadCurrencyRatesIfNecessary(state.currency, true)
            is Timed -> loadCurrencyRatesIfNecessary(state.currency)
        }
    }

    fun observeRefreshCurrencyRatesState(lifecycleOwner: LifecycleOwner) = refreshCurrencyRatesState.observe(lifecycleOwner)

    fun fetchCurrencyRates() = loadCurrencyRates(baseCurrency)

    fun currencyRateClicked(item: CurrencyRateDisplayItem) = refreshCurrencyRatesProcessor.onNext(Force(item.currencyRate.currency))

    fun inputValueChanged(item: CurrencyRateEditableItem) {
        stopTimedRefresh()
        currencyRateItems = recalculateItemsOnInputValueChanged(item)
        refreshCurrencyRatesState.onNext(RefreshedAll(currencyRateItems))
        startTimedRefreshIfNecessary()
    }

    private fun recalculateItemsOnInputValueChanged(item: CurrencyRateEditableItem) : MutableList<CurrencyRateItem> {
        val value = item.inputValue.toFloatOrZero()
        return ArrayList(currencyRateItems).map {
            if (it is CurrencyRateDisplayItem) it.copy(
                currencyRate = it.currencyRate,
                currencyValue = calculateAndFormatCurrencyValue(value, it.currencyRate.rate)
            )
            else it
        }.toMutableList()
    }

    private fun loadCurrencyRatesIfNecessary(currency: Currency, forceRefresh: Boolean = false) {
        val requestInProgress = loadCurrencyRatesDisposable.isInProgress()
        when {
            forceRefresh && requestInProgress -> {
                stopTimedRefresh()
                loadCurrencyRatesDisposable?.dispose()
            }
            forceRefresh -> stopTimedRefresh()
        }
        loadCurrencyRates(currency)
    }

    private fun loadCurrencyRates(currency: Currency) {
        loadCurrencyRatesDisposable = loadCurrencyInfoUseCase.loadCurrencyInfo(currency.name)
            .map { getRefreshCurrencyRatesSuccessState(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { state ->
                    currencyRateItems = state.currencyRates.toMutableList()
                    baseCurrency = currency
                    refreshCurrencyRatesState.onNext(state)
                    startTimedRefreshIfNecessary()
                },
                { throwable ->
                    Timber.d(throwable)
                    refreshCurrencyRatesState.onNext(handleLoadCurrencyRatesError(throwable))
                }
            )
    }

    private fun handleLoadCurrencyRatesError(throwable: Throwable) = when(throwable) {
        is NoInternetConnectionException -> NoInternetConnectionError
        else -> UnknownError
    }

    private fun getRefreshCurrencyRatesSuccessState(currencyInfo: CurrencyInfo): RefreshSuccess {
        val oldBaseCurrency = baseCurrency
        val isBaseCurrencyChanged = baseCurrencyChanged(oldBaseCurrency, currencyInfo.baseCurrency)
        return when {
            currencyRateItems.isEmpty() -> RefreshedAll(getRefreshCurrencyRatesStateListWhenIsFirstFetch(currencyInfo))
            !isBaseCurrencyChanged -> {
                val list = getRefreshCurrencyRatesStateWhenBaseCurrencyNotChanged(currencyInfo)
                RefreshAllApartFromFirst(list, 1, list.lastIndex)
            }
            else -> RefreshAllAndScrollTop(getRefreshCurrencyRatesStateWhenBaseCurrencyChanged(currencyInfo))
        }
    }

    private fun getRefreshCurrencyRatesStateListWhenIsFirstFetch(currencyInfo: CurrencyInfo): List<CurrencyRateItem> =
        mutableListOf<CurrencyRateItem>().apply {
            add(CurrencyRateEditableItem(currency = baseCurrency, focusable = true))
            addAll(currencyInfo.currencyRates.map { mapCurrencyRateDisplayItem(it, 0F) })
        }

    private fun getRefreshCurrencyRatesStateWhenBaseCurrencyNotChanged(currencyInfo: CurrencyInfo): List<CurrencyRateItem> =
        mutableListOf<CurrencyRateItem>().apply {
            val editableItemInputValue = (currencyRateItems[0] as CurrencyRateEditableItem).inputValue.toFloatOrZero()
            add(currencyRateItems[0])
            addAll(currencyInfo.currencyRates.map { mapCurrencyRateDisplayItem(it, editableItemInputValue) })
        }.sortBasedOn(currencyRateItems)


    private fun getRefreshCurrencyRatesStateWhenBaseCurrencyChanged(currencyInfo: CurrencyInfo): List<CurrencyRateItem> {
        var newBaseCurrencyIndex = 0
        val currentList = ArrayList(currencyRateItems)
        currentList.forEachIndexed { index, item ->
                if (item.provideCurrency() == currencyInfo.baseCurrency) newBaseCurrencyIndex = index
        }
        currentList.removeAt(newBaseCurrencyIndex)
        val newList = mutableListOf<CurrencyRateItem>().apply {
            addAll(currencyInfo.currencyRates.map { mapCurrencyRateDisplayItem(it, 0F) })
        }.sortBasedOn(currentList)
        return mutableListOf<CurrencyRateItem>().apply {
            addAll(newList)
            val newEditableItem = CurrencyRateEditableItem(currency = currencyInfo.baseCurrency, focusable = true, resetInputValue = true, requestFocus = true)
            add(0, newEditableItem)
        }
    }

    private fun mapCurrencyRateDisplayItem(currencyRate: CurrencyRate, baseCurrencyInputValue: Float) = CurrencyRateDisplayItem(
        currencyRate = currencyRate,
        currencyValue = calculateAndFormatCurrencyValue(baseCurrencyInputValue, currencyRate.rate)
    )

    private fun baseCurrencyChanged(oldBaseCurrency: Currency, newBaseCurrency: Currency) = oldBaseCurrency != newBaseCurrency

    private fun calculateAndFormatCurrencyValue(inputValue: Float, currencyRateValue: Float): String {
        val output = ((inputValue * currencyRateValue * 100.0).roundToInt() / 100.0).round(decimalPlaces = 2)
        return String.format("%.2f", output)
    }

    private fun startTimedRefreshIfNecessary() {
        if (timedRefreshDisposable.isDisposedOrNull())  {
            timedRefreshDisposable = Observable.interval(REFRESH_RATE, TimeUnit.SECONDS)
                .subscribe { refreshCurrencyRatesProcessor.onNext(Timed(baseCurrency)) }
        }
    }

    private fun stopTimedRefresh() = timedRefreshDisposable?.dispose()

    override fun onCleared() {
        super.onCleared()
        timedRefreshDisposable?.dispose()
        loadCurrencyRatesDisposable?.dispose()
        decideWhenToRefreshDisposable.dispose()
    }

    sealed class RefreshCurrencyRatesState {
        data class RefreshedAll(override val currencyRates: List<CurrencyRateItem>): RefreshSuccess(currencyRates)
        data class RefreshAllAndScrollTop(override val currencyRates: List<CurrencyRateItem>): RefreshSuccess(currencyRates)
        data class RefreshAllApartFromFirst(override val currencyRates: List<CurrencyRateItem>, val from: Int, val to: Int): RefreshSuccess(currencyRates)
        object NoInternetConnectionError: RefreshCurrencyRatesState()
        object UnknownError: RefreshCurrencyRatesState()
        abstract class RefreshSuccess(open val currencyRates: List<CurrencyRateItem>): RefreshCurrencyRatesState()
    }

    sealed class RefreshType(open val currency: Currency) {
        data class Timed(override val currency: Currency): RefreshType(currency)
        data class Force(override val currency: Currency): RefreshType(currency)
    }
}