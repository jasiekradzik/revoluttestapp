package pl.janradzikowski.revoluttestapp.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_currency_rates.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import pl.janradzikowski.revoluttestapp.R
import pl.janradzikowski.revoluttestapp.gone
import pl.janradzikowski.revoluttestapp.openKeyboard
import pl.janradzikowski.revoluttestapp.presentation.CurrencyRatesViewModel.RefreshCurrencyRatesState.*

class CurrencyRatesActivity : AppCompatActivity(R.layout.activity_currency_rates) {
    private val viewModel: CurrencyRatesViewModel by viewModel()
    private val adapter by lazy {
        CurrencyRateListAdapter(
            valueChanged = { item -> viewModel.inputValueChanged(item) },
            itemClicked = { viewModel.currencyRateClicked(it) }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initLayout()
        initViewModel()
    }

    private fun initLayout() {
        val layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter
        recycler.layoutManager = layoutManager
        adapter.data = viewModel.currencyRateItems
    }

    private fun initViewModel() {
        viewModel.observeRefreshCurrencyRatesState(this).subscribe { state ->
            progress.gone()
            when(state) {
                is RefreshedAll -> adapter.notifyDataSetChanged()
                is RefreshAllAndScrollTop -> {
                    adapter.notifyDataSetChanged()
                    recycler.scrollTo(0, 0)
                    openKeyboard()
                }
                is RefreshAllApartFromFirst -> adapter.notifyItemRangeChanged(state.from, state.to, CurrencyRateListAdapter.CurrencyRatePayload.DISPLAY)
                NoInternetConnectionError -> {
                    progress.gone()
                    Toast.makeText(this, "Internet error", Toast.LENGTH_LONG).show()
                }
                UnknownError -> {
                    progress.gone()
                    Toast.makeText(this, "Unknown error", Toast.LENGTH_LONG).show()
                }
            }
        }
        viewModel.fetchCurrencyRates()
    }
}