package pl.janradzikowski.revoluttestapp.infrastructure.retrofit

import com.squareup.moshi.Json
import pl.janradzikowski.revoluttestapp.infrastructure.RequestError

data class ValidationError(
    @Json(name = "message") val errorMessage: String?
) : RequestError {

    override val message: String?
        get() = this.errorMessage

}