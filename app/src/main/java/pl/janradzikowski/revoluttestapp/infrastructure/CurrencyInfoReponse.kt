package pl.janradzikowski.revoluttestapp.infrastructure

import com.squareup.moshi.Json

data class CurrencyInfoResponse(
    @Json(name = "baseCurrency") val baseCurrency: String,
    @Json(name = "rates") val currencies: Map<String, Float>
)