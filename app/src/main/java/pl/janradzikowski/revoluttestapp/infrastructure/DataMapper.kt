package pl.janradzikowski.revoluttestapp.infrastructure

interface DataMapper<From, To> {
    fun apply(from: From): To
}