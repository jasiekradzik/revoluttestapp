package pl.janradzikowski.revoluttestapp.infrastructure.retrofit

import io.reactivex.rxjava3.core.Single
import pl.janradzikowski.revoluttestapp.infrastructure.CurrencyInfoResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyInfoApi {

    @GET(value = "latest")
    fun loadCurrencyInfo(@Query("base") baseCurrencyCode: String): Single<CurrencyInfoResponse>
}