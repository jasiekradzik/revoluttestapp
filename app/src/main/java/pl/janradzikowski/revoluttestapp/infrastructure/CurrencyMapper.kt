package pl.janradzikowski.revoluttestapp.infrastructure

import pl.janradzikowski.revoluttestapp.domain.Currency

class CurrencyMapper : DataMapper<String, Currency> {
    override fun apply(from: String): Currency = when(from) {
        "AUD" -> Currency.AUD
        "EUR" -> Currency.EUR
        "BGN" -> Currency.BGN
        "BRL" -> Currency.BRL
        "CAD" -> Currency.CAD
        "CHF" -> Currency.CHF
        "CNY" -> Currency.CNY
        "CZK" -> Currency.CZK
        "DKK" -> Currency.DKK
        "GBP" -> Currency.GBP
        "HKD" -> Currency.HKD
        "HRK" -> Currency.HRK
        "HUF" -> Currency.HUF
        "IDR" -> Currency.IDR
        "ILS" -> Currency.ILS
        "INR" -> Currency.INR
        "ISK" -> Currency.ISK
        "JPY" -> Currency.JPY
        "KRW" -> Currency.KRW
        "MXN" -> Currency.MXN
        "MYR" -> Currency.MYR
        "NOK" -> Currency.NOK
        "NZD" -> Currency.NZD
        "PHP" -> Currency.PHP
        "PLN" -> Currency.PLN
        "RON" -> Currency.RON
        "RUB" -> Currency.RUB
        "SEK" -> Currency.SEK
        "SGD" -> Currency.SGD
        "THB" -> Currency.THB
        "USD" -> Currency.USD
        "ZAR" -> Currency.ZAR
        else -> throw UnknownCurrencyCodeException(from)
    }

    data class UnknownCurrencyCodeException(
        val currencyCode: String,
        override val message: String = "Couldn't map currency code: $currencyCode"
    ): IllegalArgumentException(message)
}