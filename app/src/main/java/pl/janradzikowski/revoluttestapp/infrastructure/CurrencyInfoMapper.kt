package pl.janradzikowski.revoluttestapp.infrastructure

import pl.janradzikowski.revoluttestapp.domain.CurrencyInfo
import pl.janradzikowski.revoluttestapp.domain.CurrencyRate

class CurrencyInfoMapper(
    private val currencyMapper: CurrencyMapper
) : DataMapper<CurrencyInfoResponse, CurrencyInfo> {
    override fun apply(response: CurrencyInfoResponse) =
        CurrencyInfo(
            baseCurrency = currencyMapper.apply(response.baseCurrency),
            currencyRates = mapCurrencyRates(response.currencies)
        )

    private fun mapCurrencyRates(rates: Map<String, Float>): List<CurrencyRate> =
        rates.toList().map { (currencyCode, rate) ->
            CurrencyRate(
                currency = currencyMapper.apply(currencyCode),
                rate = rate
            )
        }
}