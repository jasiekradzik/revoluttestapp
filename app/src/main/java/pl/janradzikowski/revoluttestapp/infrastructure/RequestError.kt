package pl.janradzikowski.revoluttestapp.infrastructure

interface RequestError {
    val message: String?
}
