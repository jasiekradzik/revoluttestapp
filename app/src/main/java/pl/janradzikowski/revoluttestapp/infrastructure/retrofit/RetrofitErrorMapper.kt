package pl.janradzikowski.revoluttestapp.infrastructure.retrofit

import okhttp3.ResponseBody
import pl.janradzikowski.revoluttestapp.domain.NoInternetConnectionException
import pl.janradzikowski.revoluttestapp.domain.RequestException
import pl.janradzikowski.revoluttestapp.infrastructure.DataMapper
import retrofit2.Converter
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException
import java.net.UnknownHostException

class RetrofitErrorMapper(
    private val validationErrorConverter: Converter<ResponseBody, ValidationError>
) : DataMapper<Throwable, Throwable> {

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    override fun apply(throwable: Throwable): Throwable {
        Timber.w(throwable)

        when (throwable) {

            is HttpException -> {
                throwable.response()?.let { response ->

                    val validationError = validationErrorConverter.convert(response.errorBody())
                    if (validationError != null) {
                        return RequestException(throwable, validationError, response.code())
                    }
                }
                return throwable
            }

            is UnknownHostException -> return NoInternetConnectionException(throwable)
            is IOException -> return NoInternetConnectionException(throwable)

            else -> return throwable
        }
    }
}