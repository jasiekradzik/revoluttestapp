package pl.janradzikowski.revoluttestapp.infrastructure.retrofit

import okhttp3.ResponseBody
import retrofit2.Converter

class ValidationErrorConverter(
    private val converter: Converter<ResponseBody, ValidationError>
) : Converter<ResponseBody, ValidationError> {

    @Suppress("TooGenericExceptionCaught")
    override fun convert(responseBody: ResponseBody): ValidationError? {
        return try {
            converter.convert(responseBody)
        } catch (e: Exception) {
            null
        }
    }
}