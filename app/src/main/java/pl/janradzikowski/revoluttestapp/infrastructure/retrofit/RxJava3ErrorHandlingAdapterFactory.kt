package pl.janradzikowski.revoluttestapp.infrastructure.retrofit

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import pl.janradzikowski.revoluttestapp.infrastructure.DataMapper
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import java.lang.reflect.Type

class RxJava3ErrorHandlingCallAdapterFactory(
    private val errorMapper: DataMapper<Throwable, Throwable>
) : CallAdapter.Factory() {

    private val original: RxJava3CallAdapterFactory

    init {
        this.original = RxJava3CallAdapterFactory.createWithScheduler(Schedulers.io())
    }

    @Suppress("UNCHECKED_CAST")
    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {
        return RxCallAdapterWrapper(
            retrofit,
            original.get(returnType, annotations, retrofit) as CallAdapter<CallAdapter<*, *>, Any>,
            errorMapper
        )
    }

    private class RxCallAdapterWrapper<R>(
        private val retrofit: Retrofit,
        private val wrapped: CallAdapter<R, Any>,
        private val errorMapper: DataMapper<Throwable, Throwable>
    )
        : CallAdapter<R, Any> {

        override fun responseType(): Type {
            return wrapped.responseType()
        }

        override fun adapt(call: Call<R>): Any {
            val result = wrapped.adapt(call)

            if (result is Single<*>) {
                return result.onErrorResumeNext { throwable ->
                    Single.create { emitter -> emitter.tryOnError(errorMapper.apply(throwable)) }
                }
            }

            if (result is Observable<*>) {
                return result.onErrorResumeNext { throwable: Throwable ->
                    Observable.create { emitter -> emitter.tryOnError(errorMapper.apply(throwable)) }
                }
            }

            if (result is Completable) {
                return result.onErrorResumeNext { throwable ->
                    Completable.create { emitter -> emitter.tryOnError(errorMapper.apply(throwable)) }
                }
            }

            return result
        }
    }
}