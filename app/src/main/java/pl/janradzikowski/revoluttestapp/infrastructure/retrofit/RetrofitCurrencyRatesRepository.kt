package pl.janradzikowski.revoluttestapp.infrastructure.retrofit

import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import pl.janradzikowski.revoluttestapp.domain.CurrencyInfo
import pl.janradzikowski.revoluttestapp.domain.CurrencyInfoRepository
import pl.janradzikowski.revoluttestapp.infrastructure.CurrencyInfoMapper

class RetrofitCurrencyRatesRepository(
    private val api: CurrencyInfoApi,
    private val mapper: CurrencyInfoMapper
): CurrencyInfoRepository {

    override fun loadCurrencyInfo(baseCurrencyCode: String) : Single<CurrencyInfo> = api.loadCurrencyInfo(baseCurrencyCode)
        .subscribeOn(Schedulers.io())
        .map { mapper.apply(it) }
}