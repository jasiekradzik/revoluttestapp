package pl.janradzikowski.revoluttestapp.domain

class NoInternetConnectionException : Exception {
    constructor(cause: Throwable) : super(cause)
    constructor(message: String, cause: Throwable) : super(message, cause)
}