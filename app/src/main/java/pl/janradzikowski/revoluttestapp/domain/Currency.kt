package pl.janradzikowski.revoluttestapp.domain

import androidx.annotation.DrawableRes
import pl.janradzikowski.revoluttestapp.R


data class CurrencyInfo(
    val baseCurrency: Currency,
    val currencyRates: List<CurrencyRate>
)

data class CurrencyRate(val currency: Currency, val rate: Float)

enum class Currency(val descriptionResId: Int, @DrawableRes val iconRes: Int = R.drawable.ic_temp) {
    AUD(R.string.currency_aud, R.drawable.ic_aud),
    EUR(R.string.currency_eur, R.drawable.ic_eur),
    BGN(R.string.currency_bgn, R.drawable.ic_bgn),
    BRL(R.string.currency_brl, R.drawable.ic_brl),
    CAD(R.string.currency_cad),
    CHF(R.string.currency_chf, R.drawable.ic_chf),
    CNY(R.string.currency_cny, R.drawable.ic_cny),
    CZK(R.string.currency_czk),
    DKK(R.string.currency_dkk, R.drawable.ic_dkk),
    GBP(R.string.currency_gbp),
    HKD(R.string.currency_hkd),
    HRK(R.string.currency_hrk),
    HUF(R.string.currency_huf),
    IDR(R.string.currency_idr),
    ILS(R.string.currency_ils),
    INR(R.string.currency_inr),
    ISK(R.string.currency_isk),
    JPY(R.string.currency_jpy),
    KRW(R.string.currency_krw),
    MXN(R.string.currency_mxn),
    MYR(R.string.currency_myr),
    NOK(R.string.currency_nok),
    NZD(R.string.currency_nzd),
    PHP(R.string.currency_php),
    PLN(R.string.currency_pln, R.drawable.ic_pln),
    RON(R.string.currency_ron),
    RUB(R.string.currency_rub),
    SEK(R.string.currency_sek),
    SGD(R.string.currency_sgd),
    THB(R.string.currency_thb),
    USD(R.string.currency_usd),
    ZAR(R.string.currency_zar)
}