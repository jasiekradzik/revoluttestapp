package pl.janradzikowski.revoluttestapp.domain

abstract class CurrencyRateItem {
    abstract fun provideCurrency(): Currency
    override fun equals(other: Any?) =
        when {
            other !is CurrencyRateItem -> false
            other.provideCurrency() == provideCurrency() -> true
            else -> false
        }
    override fun hashCode() = javaClass.hashCode().plus(provideCurrency().hashCode())
}

data class CurrencyRateEditableItem(
    val currency: Currency,
    var inputValue: String = "",
    var focusable: Boolean,
    var resetInputValue: Boolean = false,
    var requestFocus: Boolean = false
): CurrencyRateItem() {
    override fun provideCurrency() = currency
    override fun equals(other: Any?) =
        when {
            super.equals(other) && other is CurrencyRateEditableItem -> true
            else -> false
        }
    override fun hashCode(): Int = javaClass.hashCode().plus(super.hashCode())
}

data class CurrencyRateDisplayItem(
    val currencyRate: CurrencyRate,
    var currencyValue: String = "0"
): CurrencyRateItem() {
    override fun provideCurrency() = currencyRate.currency
    override fun equals(other: Any?) =
        when {
            super.equals(other) && other is CurrencyRateDisplayItem -> true
            else -> false
        }
    override fun hashCode(): Int = javaClass.hashCode().plus(super.hashCode())
}