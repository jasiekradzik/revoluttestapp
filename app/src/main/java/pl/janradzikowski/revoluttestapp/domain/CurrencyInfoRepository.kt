package pl.janradzikowski.revoluttestapp.domain

import io.reactivex.rxjava3.core.Single

interface CurrencyInfoRepository {
    fun loadCurrencyInfo(baseCurrencyCode: String) : Single<CurrencyInfo>
}