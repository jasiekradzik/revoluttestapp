package pl.janradzikowski.revoluttestapp.domain

class LoadCurrencyInfoUseCase(
    private val repository: CurrencyInfoRepository
) {

    fun loadCurrencyInfo(baseCurrencyCode: String) = repository.loadCurrencyInfo(baseCurrencyCode)
}