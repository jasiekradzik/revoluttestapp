package pl.janradzikowski.revoluttestapp.domain

import pl.janradzikowski.revoluttestapp.infrastructure.RequestError
import java.io.IOException

class RequestException(
    originalException: Throwable,
    val error: RequestError?,
    val status: Int
) : IOException(originalException) {

    override val message: String?
        get() = if (error?.message != null) {
            getErrorMessage(error)
        } else {
            super.message
        }

    private fun getErrorMessage(requestError: RequestError): String =
        String.format("HTTP %d: %s", status, requestError.message)

}