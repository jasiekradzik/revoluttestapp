package pl.janradzikowski.revoluttestapp

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import pl.janradzikowski.revoluttestapp.di.domainModule
import pl.janradzikowski.revoluttestapp.di.mappersModule
import pl.janradzikowski.revoluttestapp.di.retrofitModule
import pl.janradzikowski.revoluttestapp.di.viewModelsModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(retrofitModule, domainModule, viewModelsModule, mappersModule)
        }
    }
}