package pl.janradzikowski.revoluttestapp

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import io.reactivex.rxjava3.disposables.Disposable
import java.math.BigDecimal
import java.math.RoundingMode

fun <T>List<T>.sortBasedOn(list: List<T>): List<T> = sortedBy { list.indexOf(it) }

fun String?.toFloatOrZero(): Float = this?.toFloatOrNull() ?: 0F

fun Disposable?.isInProgress(): Boolean = !(this?.isDisposed ?: true)

fun View.gone() {
    visibility = View.GONE
}

fun Disposable?.isDisposedOrNull() = this == null || this.isDisposed

fun Double.round(decimalPlaces: Int): Double {
    require(decimalPlaces >= 0)
    var bd = BigDecimal.valueOf(this)
    bd = bd.setScale(decimalPlaces, RoundingMode.HALF_UP)
    return bd.toDouble()
}

fun Activity.openKeyboard() =
    (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).toggleSoftInput(
        InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)